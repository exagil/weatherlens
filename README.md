WeatherLens
===========

WeatherLens for Android provides detailed current weather observation and forecast for locations all across the world.


Screenshots (Nexus 6p)
-------------------
![Mobile_6_Port](http://res.cloudinary.com/chi6rag/image/upload/c_scale,w_400/v1472987349/weatherlens_mockup_vtwcen.png)


Build Instructions
------------------
- Clone the repository
- Install [Java](https://java.com/en/download/help/download_options.xml)
- Install [Android Studio](http://developer.android.com/sdk/index.html) with Android SDK Tools
- Setup and start an [AVD](https://developer.android.com/studio/run/managing-avds.html)
- Register for an API Key at `http://openweathermap.org/`
- Place the API Key in `app/src/debug/values/keys.xml` file besides `open_weather_map_appid` string for running the debug build
- Install the debug version of the app using `./gradlew installDebug`

Running Tests
-------------
- Run the unit tests using `./gradlew test`
- Run the instrumentation tests using `./gradlew cAT`
