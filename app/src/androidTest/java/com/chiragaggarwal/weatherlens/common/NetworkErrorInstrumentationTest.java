package com.chiragaggarwal.weatherlens.common;

import android.content.Context;
import android.support.test.InstrumentationRegistry;

import com.chiragaggarwal.weatherlens.instrumentationTesthelper.AssetUtils;

import junit.framework.Assert;

import org.junit.Test;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

import static junit.framework.Assert.assertEquals;

public class NetworkErrorInstrumentationTest {
    @Test
    public void testThatANetworkErrorIsNotEqualToNull() {
        Context context = InstrumentationRegistry.getTargetContext();
        NetworkError networkError = new NetworkError(context, new UnknownHostException("http://api.openweathermap.org/"));
        Assert.assertFalse(networkError.equals(null));
    }

    @Test
    public void testThatItIsEqualToItself() {
        Context context = InstrumentationRegistry.getTargetContext();
        NetworkError networkError = new NetworkError(context, new UnknownHostException("http://api.openweathermap.org/"));
        Assert.assertTrue(networkError.equals(networkError));
    }

    @Test
    public void testThatNetworkErrorIsEqualToAnotherNetworkErrorWithSameStates() {
        Context context = InstrumentationRegistry.getTargetContext();
        NetworkError firstNetworkError = new NetworkError(context, new UnknownHostException("http://api.openweathermap.org/"));
        NetworkError secondNetworkError = new NetworkError(context, new UnknownHostException("http://api.openweathermap.org/"));
        Assert.assertTrue(firstNetworkError.equals(secondNetworkError));
    }

    @Test
    public void testThatNetworkErrorIsNotEqualToAnotherNetworkErrorIfBothHaveDifferentCauses() {
        Context context = InstrumentationRegistry.getTargetContext();
        NetworkError firstNetworkError = new NetworkError(context, new UnknownHostException("http://api.openweathermap.org/"));
        NetworkError secondNetworkError = new NetworkError(context, new SocketTimeoutException("Could not receive response from http://api.openweathermap.org/"));
        Assert.assertFalse(firstNetworkError.equals(secondNetworkError));
    }

    @Test
    public void testThatNetworkErrorIsNotEqualToAnotherNetworkErrorIfBothHaveDifferentMessages() {
        Context context = InstrumentationRegistry.getTargetContext();
        NetworkError firstNetworkError = new NetworkError(context, new UnknownHostException("http://api.openweathermap.org/"));
        NetworkError secondNetworkError = new NetworkError(context, new UnknownHostException("http://google.com/"));
        Assert.assertFalse(firstNetworkError.equals(secondNetworkError));
    }

    @Test
    public void testThatNetworkErrorIsNotEqualToAnythingOtherThanNetworkError() {
        Context context = InstrumentationRegistry.getTargetContext();
        NetworkError networkError = new NetworkError(context, new UnknownHostException("http://api.openweathermap.org/"));
        Assert.assertFalse(networkError.equals(new Object()));
    }

    @Test
    public void testThatHttpNetworkErrorIsEqualToAnotherHttpNetworkErrorWithSameErrorMessage() {
        Context context = InstrumentationRegistry.getTargetContext();
        String content = AssetUtils.readAsset("forecast_response_failure_invalid_city.json");
        ResponseBody responseBody = ResponseBody.create(MediaType.parse("UTF-8"), content);
        Response<String> response = Response.error(404, responseBody);
        Throwable throwable = new HttpException(response);
        NetworkError actualNetworkError = NetworkError.buildFrom(context, content);
        NetworkError expectedNetworkError = new NetworkError(context, throwable);
        Assert.assertTrue(actualNetworkError.equals(expectedNetworkError));
    }

    @Test
    public void testThatHttpNetworkErrorIsNotEqualToAnotherHttpNetworkErrorWithSameMessageButDifferentCode() {
        Context context = InstrumentationRegistry.getTargetContext();
        String content = AssetUtils.readAsset("forecast_response_failure_invalid_city.json");
        ResponseBody responseBody = ResponseBody.create(MediaType.parse("UTF-8"), content);
        Response<String> response = Response.error(401, responseBody);
        Throwable throwable = new HttpException(response);
        NetworkError actualNetworkError = NetworkError.buildFrom(context, content);
        NetworkError expectedNetworkError = new NetworkError(context, throwable);
        Assert.assertFalse(actualNetworkError.equals(expectedNetworkError));
    }

    @Test
    public void testThatItKnowsHowToProvideTheErrorMessageFromInvalidResponseBody() throws IOException {
        String responseForecastFailureInvalidCity = AssetUtils.readAsset("forecast_response_failure_invalid_city.json");
        ResponseBody responseBody = ResponseBody.create(MediaType.parse("UTF-8"), responseForecastFailureInvalidCity);
        Response<String> response = Response.error(404, responseBody);
        Throwable throwable = new HttpException(response);
        NetworkError networkError = new NetworkError(InstrumentationRegistry.getTargetContext(), throwable);
        assertEquals("Error: Not found city", networkError.getMessage());
    }

    @Test
    public void testThatItKnowsHowToHandleAnInvalidError() throws IOException {
        String responseForecastFailureInvalidCity = "";
        ResponseBody responseBody = ResponseBody.create(MediaType.parse("UTF-8"), responseForecastFailureInvalidCity);
        Response<String> response = Response.error(404, responseBody);
        Throwable throwable = new HttpException(response);
        NetworkError networkError = new NetworkError(InstrumentationRegistry.getTargetContext(), throwable);
        assertEquals("Something went wrong! Please try again.", networkError.getMessage());
    }
}
