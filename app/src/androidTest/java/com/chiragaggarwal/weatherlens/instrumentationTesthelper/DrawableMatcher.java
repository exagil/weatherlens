package com.chiragaggarwal.weatherlens.instrumentationTesthelper;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.DrawableRes;
import android.view.View;
import android.widget.ImageView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class DrawableMatcher extends TypeSafeMatcher<View> {
    private int drawableResource;

    private DrawableMatcher(@DrawableRes int drawableResource) {
        super(View.class);
        this.drawableResource = drawableResource;
    }

    @Override
    protected boolean matchesSafely(View item) {
        if (!(item instanceof ImageView))
            return false;
        Resources resources = item.getResources();
        Bitmap expectedBitmap = BitmapFactory.decodeResource(resources, drawableResource);
        Bitmap actualBitmap = ((BitmapDrawable) ((ImageView) item).getDrawable()).getBitmap();
        return expectedBitmap.sameAs(actualBitmap);
    }

    @Override
    public void describeTo(Description description) {
    }

    public static Matcher<? super View> withDrawable(@DrawableRes int drawableResource) {
        return new DrawableMatcher(drawableResource);
    }
}
