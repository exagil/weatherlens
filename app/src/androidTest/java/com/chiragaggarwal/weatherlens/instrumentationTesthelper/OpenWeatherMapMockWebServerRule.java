package com.chiragaggarwal.weatherlens.instrumentationTesthelper;

import org.hamcrest.Matcher;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import java.io.IOException;

import okhttp3.Headers;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

public class OpenWeatherMapMockWebServerRule implements TestRule {
    private static final int OPEN_WEATHER_MAP_MOCK_WEB_SERVER_PORT = 4567;
    private MockHTTPDispatcher openWeatherMapMockHTTPDispatcher;
    private MockWebServer openWeatherMapMockWebServer;

    public OpenWeatherMapMockWebServerRule() {
        openWeatherMapMockWebServer = new MockWebServer();
        openWeatherMapMockHTTPDispatcher = new MockHTTPDispatcher();
        openWeatherMapMockWebServer.setDispatcher(openWeatherMapMockHTTPDispatcher);
    }

    @Override
    public Statement apply(Statement statement, Description description) {
        return new MockHTTPServerStatement(statement);
    }

    public void mock(Matcher<RecordedRequest> requestMatcher, int httpResponseCode, String response, Headers headers) throws IOException {
        MockResponse mockResponse = new MockResponse()
                .setResponseCode(httpResponseCode)
                .setBody(response);
        if (headers != null)
            mockResponse.setHeaders(headers);
        openWeatherMapMockHTTPDispatcher.mock(requestMatcher, mockResponse);
    }

    private class MockHTTPServerStatement extends Statement {

        private Statement base;

        public MockHTTPServerStatement(Statement base) {
            this.base = base;
        }

        @Override
        public void evaluate() throws Throwable {
            openWeatherMapMockWebServer.start(OPEN_WEATHER_MAP_MOCK_WEB_SERVER_PORT);
            try {
                this.base.evaluate();
            } finally {
                openWeatherMapMockWebServer.shutdown();
            }
        }
    }
}
