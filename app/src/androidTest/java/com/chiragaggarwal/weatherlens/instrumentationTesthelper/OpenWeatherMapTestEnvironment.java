package com.chiragaggarwal.weatherlens.instrumentationTesthelper;

import android.support.test.InstrumentationRegistry;

import com.chiragaggarwal.weatherlens.WeatherLensApplication;
import com.chiragaggarwal.weatherlens.dependencies.AppModule;
import com.chiragaggarwal.weatherlens.dependencies.ConfigurationModule;
import com.chiragaggarwal.weatherlens.dependencies.DaggerWeatherLensDependencies;
import com.chiragaggarwal.weatherlens.dependencies.WeatherLensDependencies;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

public class OpenWeatherMapTestEnvironment implements TestRule {
    private static final String OPEN_WEATHER_MAP_BASE_URL = "http://localhost:4567/";

    @Override
    public Statement apply(Statement base, Description description) {
        return new OpenWeatherMapTestEnvironmentStatement(base);
    }

    private class OpenWeatherMapTestEnvironmentStatement extends Statement {
        private Statement statement;

        public OpenWeatherMapTestEnvironmentStatement(Statement statement) {
            this.statement = statement;
        }

        @Override
        public void evaluate() throws Throwable {
            WeatherLensApplication application = (WeatherLensApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();
            WeatherLensDependencies dependencies = DaggerWeatherLensDependencies.builder()
                    .appModule(new AppModule(application))
                    .configurationModule(new ConfigurationModule(OPEN_WEATHER_MAP_BASE_URL))
                    .build();
            application.setWeatherLensDependencies(dependencies);
            statement.evaluate();
        }
    }
}
