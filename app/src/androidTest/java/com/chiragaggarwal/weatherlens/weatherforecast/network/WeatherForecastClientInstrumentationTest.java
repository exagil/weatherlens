package com.chiragaggarwal.weatherlens.weatherforecast.network;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import com.chiragaggarwal.weatherlens.WeatherLensApplication;
import com.chiragaggarwal.weatherlens.common.Callback;
import com.chiragaggarwal.weatherlens.common.NetworkError;
import com.chiragaggarwal.weatherlens.instrumentationTesthelper.OpenWeatherMapMockWebServerRule;
import com.chiragaggarwal.weatherlens.instrumentationTesthelper.OpenWeatherMapTestEnvironment;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecastCondition;
import com.chiragaggarwal.weatherlens.weatherforecast.persistence.LocationPreference;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Retrofit;

import static com.chiragaggarwal.weatherlens.instrumentationTesthelper.AssetUtils.readAsset;
import static com.chiragaggarwal.weatherlens.instrumentationTesthelper.RequestMatchers.httpMethodIs;
import static com.chiragaggarwal.weatherlens.instrumentationTesthelper.RequestMatchers.pathStartsWith;
import static org.hamcrest.CoreMatchers.allOf;

@RunWith(AndroidJUnit4.class)
public class WeatherForecastClientInstrumentationTest {
    private Retrofit retrofit;

    @Rule
    public OpenWeatherMapTestEnvironment openWeatherMapTestEnvironment = new OpenWeatherMapTestEnvironment();

    @Rule
    public OpenWeatherMapMockWebServerRule openWeatherMapMockWebServerRule = new OpenWeatherMapMockWebServerRule();

    @Before
    public void setup() {
        retrofit = ((WeatherLensApplication) InstrumentationRegistry.getTargetContext().getApplicationContext()).getWeatherLensDependencies().getRetrofit();
    }

    @Test
    public void testThatWeatherForecastClientDeliversAFailureResponseWhenQueriedForAnInvalidCity() throws IOException, InterruptedException {
        String failureForecastResponse = readAsset("forecast_response_failure_invalid_city.json");
        openWeatherMapMockWebServerRule.mock(
                allOf(
                        httpMethodIs("GET"),
                        pathStartsWith("/data/2.5/forecast/daily?mode=json&units=metric&cnt=6")
                ),
                200, failureForecastResponse, null
        );
        ForecastNetworkService forecastNetworkService = retrofit.create(ForecastNetworkService.class);
        Callback weatherForecastCallback = Mockito.mock(Callback.class);
        Context context = InstrumentationRegistry.getTargetContext();
        LocationPreference locationPreference = new LocationPreference(context);
        WeatherForecastClient weatherForecastClient = new WeatherForecastClient(context, forecastNetworkService, locationPreference);
        weatherForecastClient.getWeatherForecasts(weatherForecastCallback);
        Thread.sleep(1000);
        Mockito.verify(weatherForecastCallback).onFailure(NetworkError.buildFrom(InstrumentationRegistry.getTargetContext(), failureForecastResponse));
    }

    @Test
    public void testThatWeatherForecastClientDeliversASuccessfulResponseWhenQueriedForAnValidCity() throws IOException, InterruptedException {
        String forecastResponse = readAsset("forecast_response_ok.json");
        List<WeatherForecast> weatherForecasts = buildWeatherForecasts();
        openWeatherMapMockWebServerRule.mock(
                allOf(
                        httpMethodIs("GET"),
                        pathStartsWith("/data/2.5/forecast/daily?mode=json&units=metric&cnt=6")
                ),
                200, forecastResponse, null
        );
        ForecastNetworkService forecastNetworkService = retrofit.create(ForecastNetworkService.class);
        Callback weatherForecastCallback = Mockito.mock(Callback.class);
        Context context = InstrumentationRegistry.getTargetContext();
        LocationPreference locationPreference = new LocationPreference(context);
        WeatherForecastClient weatherForecastClient = new WeatherForecastClient(context, forecastNetworkService, locationPreference);
        weatherForecastClient.getWeatherForecasts(weatherForecastCallback);
        Thread.sleep(1000);
        Mockito.verify(weatherForecastCallback).onSuccess(weatherForecasts);
    }

    private List<WeatherForecast> buildWeatherForecasts() {
        Date firstDate = new Date();
        firstDate.setTime(1472450400000l);
        WeatherForecast firstWeatherForecast = new WeatherForecast(firstDate, 26d, 26d, 25.16d, WeatherForecastCondition.GOOD, "Overcast Clouds ");
        Date secondDate = new Date();
        secondDate.setTime(1472536800000l);
        WeatherForecast secondWeatherForecast = new WeatherForecast(secondDate, 27.31d, 30.07d, 23.63d, WeatherForecastCondition.AVERAGE, "Heavy Intensity Rain ");
        Date thirdDate = new Date();
        thirdDate.setTime(1472623200000l);
        WeatherForecast thirdWeatherForecast = new WeatherForecast(thirdDate, 28.89d, 29.76d, 23.57d, WeatherForecastCondition.AVERAGE, "Heavy Intensity Rain ");
        Date fourthDate = new Date();
        fourthDate.setTime(1472709600000l);
        WeatherForecast fourthhWeatherForecast = new WeatherForecast(fourthDate, 28.14d, 30.02d, 24.68d, WeatherForecastCondition.AVERAGE, "Heavy Intensity Rain ");
        Date fifthDate = new Date();
        fifthDate.setTime(1472796000000l);
        WeatherForecast fifthWeatherForecast = new WeatherForecast(fifthDate, 28.24d, 28.24d, 24.43d, WeatherForecastCondition.AVERAGE, "Heavy Intensity Rain ");
        Date sixthDate = new Date();
        sixthDate.setTime(1472882400000l);
        WeatherForecast sixthWeatherForecast = new WeatherForecast(sixthDate, 29.24d, 32.24d, 23.43d, WeatherForecastCondition.BAD, "Heavy Snow ");

        ArrayList<WeatherForecast> weatherForecasts = new ArrayList<>();
        weatherForecasts.add(firstWeatherForecast);
        weatherForecasts.add(secondWeatherForecast);
        weatherForecasts.add(thirdWeatherForecast);
        weatherForecasts.add(fourthhWeatherForecast);
        weatherForecasts.add(fifthWeatherForecast);
        weatherForecasts.add(sixthWeatherForecast);
        return weatherForecasts;
    }
}
