package com.chiragaggarwal.weatherlens.weatherforecast.network;

import com.chiragaggarwal.weatherlens.instrumentationTesthelper.AssetUtils;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecastCondition;

import junit.framework.Assert;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class WeatherForecastConditionParserTest {
    @Test
    public void testThatItAnalysesThunderstormAsBadWeatherForecastCondition() throws JSONException {
        String forecastWeatherInfoThunderstorm = AssetUtils.readAsset("weatherinformation/forecast_weather_info_thunderstorm.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoThunderstorm);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals(WeatherForecastCondition.BAD, weatherForecastConditionParser.parseQuality());
    }

    @Test
    public void testThatItAnalysesDrizzleAsGoodWeatherForecastCondition() throws JSONException {
        String forecastWeatherInfoDrizzle = AssetUtils.readAsset("weatherinformation/forecast_weather_info_drizzle.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoDrizzle);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals(WeatherForecastCondition.AVERAGE, weatherForecastConditionParser.parseQuality());
    }

    @Test
    public void testThatItAnalysesAtmosphereAsBadWeatherForecastCondition() throws JSONException {
        String forecastWeatherInfoAtmosphere = AssetUtils.readAsset("weatherinformation/forecast_weather_info_atmosphere.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoAtmosphere);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals(WeatherForecastCondition.BAD, weatherForecastConditionParser.parseQuality());
    }

    @Test
    public void testThatItAnalysesRainAsGoodWeatherForecastCondition() throws JSONException {
        String forecastWeatherInfoRain = AssetUtils.readAsset("weatherinformation/forecast_weather_info_rain.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoRain);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals(WeatherForecastCondition.AVERAGE, weatherForecastConditionParser.parseQuality());
    }

    @Test
    public void testThatItAnalysesSnowAsGoodWeatherForecastCondition() throws JSONException {
        String forecastWeatherInfoSnow = AssetUtils.readAsset("weatherinformation/forecast_weather_info_snow.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoSnow);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals(WeatherForecastCondition.BAD, weatherForecastConditionParser.parseQuality());
    }

    @Test
    public void testThatItAnalysesClearAsGoodWeatherForecastCondition() throws JSONException {
        String forecastWeatherInfoClear = AssetUtils.readAsset("weatherinformation/forecast_weather_info_clear.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoClear);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals(WeatherForecastCondition.GOOD, weatherForecastConditionParser.parseQuality());
    }

    @Test
    public void testThatItAnalysesExtremeAsBadWeatherForecastCondition() throws JSONException {
        String forecastWeatherInfoExtreme = AssetUtils.readAsset("weatherinformation/forecast_weather_info_extreme.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoExtreme);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals(WeatherForecastCondition.BAD, weatherForecastConditionParser.parseQuality());
    }

    @Test
    public void testThatItAnalysesAdditionalAsBadWeatherForecastCondition() throws JSONException {
        String forecastWeatherInfoAdditional = AssetUtils.readAsset("weatherinformation/forecast_weather_info_additional.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoAdditional);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals(WeatherForecastCondition.BAD, weatherForecastConditionParser.parseQuality());
    }

    @Test
    public void testThatItKnowsHowToParseValidWeatherForecastDescription() throws JSONException {
        String forecastWeatherInfoThunderstorm = AssetUtils.readAsset("weatherinformation/forecast_weather_info_thunderstorm.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoThunderstorm);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals("Thunderstorm With Light Rain ", weatherForecastConditionParser.parseDescription());
    }

    @Test
    public void testThatItKnowsHowToParseInvalidWeatherForecastDescription() throws JSONException {
        String forecastWeatherInfoThunderstorm = AssetUtils.readAsset("weatherinformation/forecast_weather_invalid_info_thunderstorm.json");
        JSONObject weatherInformationJson = new JSONObject(forecastWeatherInfoThunderstorm);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformationJson);
        Assert.assertEquals("", weatherForecastConditionParser.parseDescription());
    }
}