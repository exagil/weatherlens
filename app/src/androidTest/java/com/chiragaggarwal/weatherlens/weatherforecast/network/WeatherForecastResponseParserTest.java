package com.chiragaggarwal.weatherlens.weatherforecast.network;

import com.chiragaggarwal.weatherlens.instrumentationTesthelper.AssetUtils;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecastCondition;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Date;

public class WeatherForecastResponseParserTest {
    @Test
    public void testThatItKnowsHowToSuccessfullyParseWeatherForecastsResponse() {
        String weatherForecastResponseJson = AssetUtils.readAsset("forecast_response_ok.json");
        WeatherForecastResponseParser weatherForecastResponseParser = new WeatherForecastResponseParser(weatherForecastResponseJson);
        Date firstDate = new Date();
        firstDate.setTime(1472450400000l);
        WeatherForecast firstWeatherForecast = new WeatherForecast(firstDate, 26d, 26d, 25.16d, WeatherForecastCondition.GOOD, "Overcast Clouds ");
        Date secondDate = new Date();
        secondDate.setTime(1472536800000l);
        WeatherForecast secondWeatherForecast = new WeatherForecast(secondDate, 27.31d, 30.07d, 23.63d, WeatherForecastCondition.AVERAGE, "Heavy Intensity Rain ");
        Date thirdDate = new Date();
        thirdDate.setTime(1472623200000l);
        WeatherForecast thirdWeatherForecast = new WeatherForecast(thirdDate, 28.89d, 29.76d, 23.57d, WeatherForecastCondition.AVERAGE, "Heavy Intensity Rain ");
        Date fourthDate = new Date();
        fourthDate.setTime(1472709600000l);
        WeatherForecast fourthhWeatherForecast = new WeatherForecast(fourthDate, 28.14d, 30.02d, 24.68d, WeatherForecastCondition.AVERAGE, "Heavy Intensity Rain ");
        Date fifthDate = new Date();
        fifthDate.setTime(1472796000000l);
        WeatherForecast fifthWeatherForecast = new WeatherForecast(fifthDate, 28.24d, 28.24d, 24.43d, WeatherForecastCondition.AVERAGE, "Heavy Intensity Rain ");
        Date sixthDate = new Date();
        sixthDate.setTime(1472882400000l);
        WeatherForecast sixthWeatherForecast = new WeatherForecast(sixthDate, 29.24d, 32.24d, 23.43d, WeatherForecastCondition.BAD, "Heavy Snow ");
        ArrayList<WeatherForecast> weatherForecasts = new ArrayList<>();
        weatherForecasts.add(firstWeatherForecast);
        weatherForecasts.add(secondWeatherForecast);
        weatherForecasts.add(thirdWeatherForecast);
        weatherForecasts.add(fourthhWeatherForecast);
        weatherForecasts.add(fifthWeatherForecast);
        weatherForecasts.add(sixthWeatherForecast);
        Assert.assertEquals(weatherForecasts, weatherForecastResponseParser.parseWeatherForecasts());
    }

    @Test
    public void testThatItParsesMalformedWeatherForecastResponseIntoAnEmptyList() {
        String weatherForecastResponseJson = AssetUtils.readAsset("forecast_response_malformed.json");
        WeatherForecastResponseParser weatherForecastResponseParser = new WeatherForecastResponseParser(weatherForecastResponseJson);
        Assert.assertEquals(new ArrayList<WeatherForecast>(), weatherForecastResponseParser.parseWeatherForecasts());
    }
}
