package com.chiragaggarwal.weatherlens.weatherforecast.network;

import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static com.chiragaggarwal.weatherlens.instrumentationTesthelper.AssetUtils.readAsset;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class WeatherForecastResponseTest {
    @Test
    public void testThatErroneousForecastResponseIsNotSuccessful() {
        String serializedWeatherForecast = readAsset("forecast_response_failure_invalid_city.json");
        WeatherForecastResponse weatherForecastResponse = new WeatherForecastResponse(serializedWeatherForecast);
        assertFalse(weatherForecastResponse.isSuccessful());
    }

    @Test
    public void testThatNonErroneousForecastResponseIsSuccessful() {
        String serializedWeatherForecast = readAsset("forecast_response_ok.json");
        WeatherForecastResponse weatherForecastResponse = new WeatherForecastResponse(serializedWeatherForecast);
        assertTrue(weatherForecastResponse.isSuccessful());
    }

    @Test
    public void testThatNonErroneousForecastResponseWithSuccessCodeButNot200IsSuccessful() {
        String serializedWeatherForecast = readAsset("forecast_response_accepted.json");
        WeatherForecastResponse weatherForecastResponse = new WeatherForecastResponse(serializedWeatherForecast);
        assertTrue(weatherForecastResponse.isSuccessful());
    }

    @Test
    public void testThatNonErroneousForecastResponseWithRedirectedCodeIsSuccessful() {
        String serializedWeatherForecast = readAsset("forecast_response_redirected.json");
        WeatherForecastResponse weatherForecastResponse = new WeatherForecastResponse(serializedWeatherForecast);
        assertTrue(weatherForecastResponse.isSuccessful());
    }

    @Test
    public void testThatMalformedForecastResponseIsNotSuccessful() {
        String malformedForecastResponse = "{/kjhfs:jd, cod: \"200\"}";
        WeatherForecastResponse weatherForecastResponse = new WeatherForecastResponse(malformedForecastResponse);
        assertFalse(weatherForecastResponse.isSuccessful());
    }
}
