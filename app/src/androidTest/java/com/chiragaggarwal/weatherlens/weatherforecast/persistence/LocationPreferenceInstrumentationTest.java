package com.chiragaggarwal.weatherlens.weatherforecast.persistence;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import junit.framework.Assert;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class LocationPreferenceInstrumentationTest {
    private Context context;
    private SharedPreferences sharedPreferences;

    @Before
    public void setup() {
        context = InstrumentationRegistry.getTargetContext();
        sharedPreferences = context.getSharedPreferences("location_preference", Context.MODE_PRIVATE);
        sharedPreferences.edit().clear().apply();
    }

    @After
    public void teardown() {
        sharedPreferences.edit().clear().apply();
    }

    @Test
    public void testThatBerlinIsSelectedAsCurrentLocationWhenNoLocationExists() {
        LocationPreference locationPreference = new LocationPreference(context);
        Assert.assertEquals("Berlin", locationPreference.current());
    }

    @Test
    public void testThatUserSelectedLocationIsTheCurrentLocationWhenItLocationExists() {
        LocationPreference locationPreference = new LocationPreference(context);
        sharedPreferences.edit().putString("current_location", "Delhi").commit();
        Assert.assertEquals("Delhi", locationPreference.current());
    }

    @Test
    public void testThatItKnowsHowToUpdateTheCurrentLocation() {
        LocationPreference locationPreference = new LocationPreference(context);
        locationPreference.set("Delhi");
        Assert.assertEquals("Delhi", locationPreference.current());
    }

    @Test
    public void testThatLocationPreferenceCanBeCleared() {
        LocationPreference locationPreference = new LocationPreference(context);
        locationPreference.set("Delhi");
        locationPreference.clear();
        Assert.assertEquals("Berlin", locationPreference.current());
    }
}
