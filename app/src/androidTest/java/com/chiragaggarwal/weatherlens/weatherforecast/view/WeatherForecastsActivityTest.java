package com.chiragaggarwal.weatherlens.weatherforecast.view;

import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.chiragaggarwal.weatherlens.R;
import com.chiragaggarwal.weatherlens.WeatherLensApplication;
import com.chiragaggarwal.weatherlens.instrumentationTesthelper.AssetUtils;
import com.chiragaggarwal.weatherlens.instrumentationTesthelper.OpenWeatherMapMockWebServerRule;
import com.chiragaggarwal.weatherlens.instrumentationTesthelper.OpenWeatherMapTestEnvironment;
import com.chiragaggarwal.weatherlens.weatherforecast.persistence.LocationPreference;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.chiragaggarwal.weatherlens.instrumentationTesthelper.DrawableMatcher.withDrawable;
import static com.chiragaggarwal.weatherlens.instrumentationTesthelper.RequestMatchers.httpMethodIs;
import static com.chiragaggarwal.weatherlens.instrumentationTesthelper.RequestMatchers.pathStartsWith;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class WeatherForecastsActivityTest {
    private LocationPreference locationPreference;

    @Rule
    public OpenWeatherMapTestEnvironment openWeatherMapTestEnvironment = new OpenWeatherMapTestEnvironment();

    @Rule
    public OpenWeatherMapMockWebServerRule openWeatherMapMockWebServerRule = new OpenWeatherMapMockWebServerRule();

    @Rule
    public ActivityTestRule<WeatherForecastsActivity> weatherForecastsActivityActivityTestRule = new ActivityTestRule<>(WeatherForecastsActivity.class, true, false);

    @Before
    public void setup() {
        locationPreference = ((WeatherLensApplication) InstrumentationRegistry.getTargetContext().getApplicationContext()).getWeatherLensDependencies().getLocationPreference();
        locationPreference.clear();
        locationPreference.set("Delhi");
    }

    @After
    public void tearDown() {
        locationPreference.clear();
    }

    @Test
    public void testThatTodaysWeatherForecastsAreLoadedSuccessfully() throws IOException, InterruptedException {
        String weatherForecastResponse = AssetUtils.readAsset("forecast_response_ok.json");
        openWeatherMapMockWebServerRule.mock(
                allOf(
                        httpMethodIs("GET"),
                        pathStartsWith("/data/2.5/forecast/daily?mode=json&units=metric&cnt=6")
                ),
                200, weatherForecastResponse, null
        );
        weatherForecastsActivityActivityTestRule.launchActivity(null);
        Thread.sleep(1000);
        onView(withId(R.id.text_weather_description)).check(matches(withText("Overcast Clouds ")));
        onView(withId(R.id.text_minimum_temperature)).check(matches(withText("25°")));
        onView(withId(R.id.text_maximum_temperature)).check(matches(withText("26°")));
        onView(withId(R.id.text_location_name)).check(matches(withText("Delhi")));
        onView(withId(R.id.text_temperature_day)).check(matches(withText("26°")));
        onView(withId(R.id.image_mood_today)).check(matches(withDrawable(R.drawable.icon_today_happy)));
    }
}
