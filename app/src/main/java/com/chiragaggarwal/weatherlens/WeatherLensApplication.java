package com.chiragaggarwal.weatherlens;

import android.app.Application;

import com.chiragaggarwal.weatherlens.dependencies.AppModule;
import com.chiragaggarwal.weatherlens.dependencies.DaggerWeatherLensDependencies;
import com.chiragaggarwal.weatherlens.dependencies.WeatherLensDependencies;

public class WeatherLensApplication extends Application {
    private WeatherLensDependencies weatherLensDependencies;

    @Override
    public void onCreate() {
        super.onCreate();
        weatherLensDependencies = DaggerWeatherLensDependencies.builder().
                appModule(new AppModule(this)).
                build();
    }

    public WeatherLensDependencies getWeatherLensDependencies() {
        return weatherLensDependencies;
    }

    public void setWeatherLensDependencies(WeatherLensDependencies dependencies) {
        weatherLensDependencies = dependencies;
    }
}
