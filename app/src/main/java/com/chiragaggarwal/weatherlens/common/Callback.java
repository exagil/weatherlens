package com.chiragaggarwal.weatherlens.common;

public interface Callback<T1, T2> {
    void onSuccess(T1 t);

    void onFailure(T2 t2);
}
