package com.chiragaggarwal.weatherlens.common;

import android.content.Context;
import android.support.annotation.NonNull;

import com.chiragaggarwal.weatherlens.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

public class NetworkError {
    public static final String KEY_CODE = "cod";
    public static final String KEY_MESSAGE = "message";
    private Context applicationContext;
    private Throwable throwable;

    public NetworkError(Context applicationContext, Throwable throwable) {
        this.applicationContext = applicationContext;
        this.throwable = throwable;
    }

    public String getMessage() {
        if (throwable instanceof IOException)
            return applicationContext.getString(R.string.error_no_internet_connection);
        String errorBodyString;
        try {
            String errorBody = ((HttpException) throwable).response().errorBody().string();
            JSONObject errorJson = new JSONObject(errorBody);
            errorBodyString = errorJson.getString(KEY_MESSAGE);
        } catch (Exception e) {
            errorBodyString = applicationContext.getString(R.string.error_something_went_wrong);
        }
        return errorBodyString;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || !(obj instanceof NetworkError)) return false;
        NetworkError thatNetworkError = (NetworkError) obj;
        if (areHttpExceptions(thatNetworkError)) {
            HttpException thisHttpException = (HttpException) this.throwable;
            HttpException thatHttpException = (HttpException) thatNetworkError.throwable;
            return doesHaveSameCode(thisHttpException, thatHttpException);
        }
        return doBothHaveSameCauses(thatNetworkError) && doBothHaveSameMessages(thatNetworkError);
    }

    private boolean doesHaveSameCode(HttpException thisHttpException, HttpException thatHttpException) {
        return thisHttpException.code() == thatHttpException.code();
    }

    private boolean areHttpExceptions(NetworkError thatNetworkError) {
        return this.throwable instanceof HttpException && thatNetworkError.throwable instanceof HttpException;
    }

    private boolean doBothHaveSameMessages(NetworkError thatNetworkError) {
        return this.throwable.getMessage().equals(thatNetworkError.throwable.getMessage());
    }

    private boolean doBothHaveSameCauses(NetworkError thatNetworkError) {
        return this.throwable.getClass().getSimpleName().equals(thatNetworkError.throwable.getClass().getSimpleName());
    }

    public static NetworkError buildFrom(Context context, String errorResponseString) {
        try {
            return buildNetworkError(context, errorResponseString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new NetworkError(context, new Throwable());
    }

    @NonNull
    private static NetworkError buildNetworkError(Context context, String errorResponseString) throws JSONException {
        JSONObject errorResponseJson = new JSONObject(errorResponseString);
        int errorResponseCode = errorResponseJson.getInt(KEY_CODE);
        ResponseBody responseBody = ResponseBody.create(MediaType.parse("UTF-8"), errorResponseString);
        Response<String> response = Response.error(errorResponseCode, responseBody);
        HttpException httpException = new HttpException(response);
        return new NetworkError(context, httpException);
    }
}
