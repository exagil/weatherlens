package com.chiragaggarwal.weatherlens.common;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class OpenWeatherMapRequestInterceptor implements Interceptor {
    private static final String APP_ID_KEY = "appid";
    private String openWeatherMapAppId;

    public OpenWeatherMapRequestInterceptor(String openWeatherMapAppId) {
        this.openWeatherMapAppId = openWeatherMapAppId;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        HttpUrl updatedUrl = request.url().newBuilder().addQueryParameter(APP_ID_KEY, openWeatherMapAppId).build();
        Request build = request.newBuilder().url(updatedUrl).build();
        return chain.proceed(build);
    }
}
