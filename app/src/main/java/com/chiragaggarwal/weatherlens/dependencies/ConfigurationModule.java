package com.chiragaggarwal.weatherlens.dependencies;

import android.content.Context;
import android.support.annotation.VisibleForTesting;

import com.chiragaggarwal.weatherlens.R;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ConfigurationModule {
    private String openWeatherMapBaseUrl;

    @SuppressWarnings("usedByDagger")
    public ConfigurationModule() {
    }

    @VisibleForTesting
    public ConfigurationModule(String openWeatherMapBaseUrl) {
        this.openWeatherMapBaseUrl = openWeatherMapBaseUrl;
    }

    @Provides
    @Singleton
    public Configuration providesEnvironmentConfiguration(Context context) {
        String openWeatherMapBaseUrl = this.openWeatherMapBaseUrl == null ? context.getString(R.string.base_url) : this.openWeatherMapBaseUrl;
        return new Configuration(openWeatherMapBaseUrl);
    }
}
