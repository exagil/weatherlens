package com.chiragaggarwal.weatherlens.dependencies;

import android.content.Context;

import com.chiragaggarwal.weatherlens.weatherforecast.persistence.LocationPreference;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class LocalStorageModule {
    @Provides
    @Singleton
    public LocationPreference providesLocationPreference(Context context) {
        return new LocationPreference(context);
    }
}
