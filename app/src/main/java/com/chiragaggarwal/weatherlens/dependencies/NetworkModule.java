package com.chiragaggarwal.weatherlens.dependencies;

import android.content.Context;

import com.chiragaggarwal.weatherlens.R;
import com.chiragaggarwal.weatherlens.common.HttpLoggingInterceptor;
import com.chiragaggarwal.weatherlens.common.OpenWeatherMapRequestInterceptor;
import com.chiragaggarwal.weatherlens.weatherforecast.network.ForecastNetworkService;
import com.chiragaggarwal.weatherlens.weatherforecast.network.WeatherForecastClient;
import com.chiragaggarwal.weatherlens.weatherforecast.persistence.LocationPreference;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

@Module
public class NetworkModule {
    @Provides
    @Singleton
    public OkHttpClient providesOkHttpClient(Context context) {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor())
                .addInterceptor(new OpenWeatherMapRequestInterceptor(context.getString(R.string.open_weather_map_appid)))
                .build();
    }

    @Provides
    @Singleton
    public Retrofit providesRetrofit(Configuration configuration, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(configuration.getOpenWeatherMapBaseUrl())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public ForecastNetworkService providesForecastNetworkService(Retrofit retrofit) {
        return retrofit.create(ForecastNetworkService.class);
    }

    @Provides
    @Singleton
    public WeatherForecastClient providesWeatherForecastClient(Context applicationContext, ForecastNetworkService forecastNetworkService, LocationPreference locationPreference) {
        return new WeatherForecastClient(applicationContext, forecastNetworkService, locationPreference);
    }
}
