package com.chiragaggarwal.weatherlens.dependencies;

import com.chiragaggarwal.weatherlens.weatherforecast.persistence.LocationPreference;
import com.chiragaggarwal.weatherlens.weatherforecast.view.WeatherForecastsActivity;

import javax.inject.Singleton;

import dagger.Component;
import retrofit2.Retrofit;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class, ConfigurationModule.class, LocalStorageModule.class})
public interface WeatherLensDependencies {
    Retrofit getRetrofit();

    LocationPreference getLocationPreference();

    void inject(WeatherForecastsActivity weatherForecastsActivity);
}
