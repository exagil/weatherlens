package com.chiragaggarwal.weatherlens.weatherforecast.models;

import android.support.annotation.DrawableRes;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

// WeatherForecast is a prediction of the state of the atmospehre

public class WeatherForecast {
    public static final String FORMAT_TEMPERATURE = "%2d";
    public static final String FORMAT_DAY = "EEE";
    public static final String UNICODE_DEGREE = "°";
    private Date date;
    private double dayTemperature;
    private double minimumTemperature;
    private double maximumTemperature;
    private final WeatherForecastCondition weatherForecastCondition;
    public final String weatherForecastDescription;

    public WeatherForecast(Date date, double dayTemperature, double maximumTemperature, double minimumTemperature, WeatherForecastCondition weatherForecastCondition, String weatherForecastDescription) {
        this.date = date;
        this.dayTemperature = dayTemperature;
        this.minimumTemperature = minimumTemperature;
        this.maximumTemperature = maximumTemperature;
        this.weatherForecastCondition = weatherForecastCondition;
        this.weatherForecastDescription = weatherForecastDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WeatherForecast that = (WeatherForecast) o;

        if (Double.compare(that.dayTemperature, dayTemperature) != 0) return false;
        if (Double.compare(that.minimumTemperature, minimumTemperature) != 0) return false;
        if (Double.compare(that.maximumTemperature, maximumTemperature) != 0) return false;
        if (date != null ? !date.equals(that.date) : that.date != null) return false;
        if (weatherForecastCondition != that.weatherForecastCondition) return false;
        return weatherForecastDescription != null ? weatherForecastDescription.equals(that.weatherForecastDescription) : that.weatherForecastDescription == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = date != null ? date.hashCode() : 0;
        temp = Double.doubleToLongBits(dayTemperature);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(minimumTemperature);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(maximumTemperature);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (weatherForecastCondition != null ? weatherForecastCondition.hashCode() : 0);
        result = 31 * result + (weatherForecastDescription != null ? weatherForecastDescription.hashCode() : 0);
        return result;
    }

    @DrawableRes
    public int moodResource() {
        return weatherForecastCondition.moodResource();
    }

    @DrawableRes
    public int todaysMoodResource() {
        return weatherForecastCondition.todaysMoodResource();
    }

    public String minimumTemperature() {
        return formatTemperature(minimumTemperature);
    }

    public String maximumTemperature() {
        return formatTemperature(maximumTemperature);
    }

    public String dayTemperature() {
        return formatTemperature(dayTemperature);
    }

    private String formatTemperature(double temperature) {
        return String.format(Locale.ENGLISH, FORMAT_TEMPERATURE + UNICODE_DEGREE, (int) temperature);
    }

    public String formattedDay() {
        SimpleDateFormat dayFormat = new SimpleDateFormat(FORMAT_DAY, Locale.ENGLISH);
        return dayFormat.format(this.date);
    }
}
