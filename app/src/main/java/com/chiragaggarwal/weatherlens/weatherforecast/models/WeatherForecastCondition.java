package com.chiragaggarwal.weatherlens.weatherforecast.models;

import android.support.annotation.DrawableRes;

import com.chiragaggarwal.weatherlens.R;

public enum WeatherForecastCondition {
    GOOD {
        public @DrawableRes int moodResource() {
            return R.drawable.icon_happy;
        }

        public @DrawableRes int todaysMoodResource() {
            return R.drawable.icon_today_happy;
        }
    },
    BAD {
        public @DrawableRes int moodResource() {
            return R.drawable.icon_sad;
        }

        public @DrawableRes int todaysMoodResource() {
            return R.drawable.icon_today_sad;
        }
    },
    AVERAGE {
        public @DrawableRes int moodResource() {
            return R.drawable.icon_whatever;
        }

        public @DrawableRes int todaysMoodResource() {
            return R.drawable.icon_today_whatever;
        }
    };

    public @DrawableRes int moodResource() {
        return this.moodResource();
    }

    public @DrawableRes int todaysMoodResource() {
        return this.todaysMoodResource();
    }
}
