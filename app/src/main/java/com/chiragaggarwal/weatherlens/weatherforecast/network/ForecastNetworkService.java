package com.chiragaggarwal.weatherlens.weatherforecast.network;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface ForecastNetworkService {
    @GET("data/2.5/forecast/daily?mode=json&units=metric&cnt=6")
    Observable<String> getForecasts(@Query("q") String locationName);
}
