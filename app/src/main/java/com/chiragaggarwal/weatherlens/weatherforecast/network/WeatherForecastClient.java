package com.chiragaggarwal.weatherlens.weatherforecast.network;

import android.content.Context;

import com.chiragaggarwal.weatherlens.common.Callback;
import com.chiragaggarwal.weatherlens.common.NetworkError;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;
import com.chiragaggarwal.weatherlens.weatherforecast.persistence.LocationPreference;

import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class WeatherForecastClient {
    public static final long DELAY_FORECASTS = 500l;
    private Context context;
    private ForecastNetworkService forecastNetworkService;
    private LocationPreference locationPreference;

    public WeatherForecastClient(Context context, ForecastNetworkService forecastNetworkService, LocationPreference locationPreference) {
        this.context = context;
        this.forecastNetworkService = forecastNetworkService;
        this.locationPreference = locationPreference;
    }

    public void getWeatherForecasts(final Callback<List<WeatherForecast>, NetworkError> weatherForecastCallback) {
        forecastNetworkService.getForecasts(locationPreference.current())
                .delay(DELAY_FORECASTS, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable error) {
                        weatherForecastCallback.onFailure(new NetworkError(context, error));
                    }

                    @Override
                    public void onNext(String weatherForecastResponseJson) {
                        WeatherForecastResponse weatherForecastResponse = new WeatherForecastResponse(weatherForecastResponseJson);
                        if (weatherForecastResponse.isSuccessful()) {
                            WeatherForecastResponseParser parser = new WeatherForecastResponseParser(weatherForecastResponseJson);
                            weatherForecastCallback.onSuccess(parser.parseWeatherForecasts());
                        } else {
                            NetworkError networkError = NetworkError.buildFrom(context, weatherForecastResponseJson);
                            weatherForecastCallback.onFailure(networkError);
                        }
                    }
                });
    }
}
