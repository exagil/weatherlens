package com.chiragaggarwal.weatherlens.weatherforecast.network;

import android.support.annotation.NonNull;

import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecastCondition;

import org.json.JSONException;
import org.json.JSONObject;

// WeatherForecastConditionParser categorizes weather forecast conditions in favor
// to the likes of a common man

public class WeatherForecastConditionParser {
    public static final String KEY_ID = "id";
    public static final int CATEGORY_DRIZZLE = 3;
    public static final int CATEGORY_RAIN = 5;
    public static final int CATEGORY_CLEAR_CLOUDS = 8;
    public static final String KEY_DESCRIPTION = "description";
    public static final String SPACE = " ";
    private static final String BLANK = "";
    private JSONObject weatherInformationJson;

    public WeatherForecastConditionParser(JSONObject weatherInformationJson) {
        this.weatherInformationJson = weatherInformationJson;
    }

    public WeatherForecastCondition parseQuality() throws JSONException {
        int weatherInformationId = this.weatherInformationJson.getInt(KEY_ID);
        int weatherInformationCategoryId = getFirstNumber(weatherInformationId);
        if (weatherInformationCategoryId == CATEGORY_CLEAR_CLOUDS)
            return WeatherForecastCondition.GOOD;
        else if (weatherInformationCategoryId == CATEGORY_DRIZZLE || weatherInformationCategoryId == CATEGORY_RAIN)
            return WeatherForecastCondition.AVERAGE;
        return WeatherForecastCondition.BAD;
    }

    private int getFirstNumber(int weatherInformationId) {
        return weatherInformationId / 100;
    }

    public String parseDescription() {
        String weatherDescription = null;
        try {
            weatherDescription = weatherInformationJson.getString(KEY_DESCRIPTION);
            StringBuilder finalDescriptionBuilder = capitalizeParsedDescription(weatherDescription);
            return finalDescriptionBuilder.toString();
        } catch (JSONException e) {
            return BLANK;
        }
    }

    @NonNull
    private StringBuilder capitalizeParsedDescription(String weatherDescription) {
        StringBuilder finalDescriptionBuilder = new StringBuilder();
        String[] weatherDescriptionWords = weatherDescription.split("\\s+");
        for (String weatherDescriptionWord : weatherDescriptionWords) {
            char firstCharacterOfWord = weatherDescriptionWord.charAt(0);
            String capitalWord = Character.toUpperCase(firstCharacterOfWord) + weatherDescriptionWord.substring(1);
            finalDescriptionBuilder.append(capitalWord + SPACE);
        }
        return finalDescriptionBuilder;
    }
}
