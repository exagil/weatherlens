package com.chiragaggarwal.weatherlens.weatherforecast.network;

import org.json.JSONException;
import org.json.JSONObject;

// WeatherForecastResponse knows if OpenWeatherMap returns a successful response

public class WeatherForecastResponse {
    public static final String KEY_CODE = "cod";
    public static final String CODE_PREFIX_SUCCESS = "2";
    public static final String CODE_PREFIX_REDIRECT = "3";
    private String serializedWeatherForecast;

    public WeatherForecastResponse(String serializedWeatherForecastResponse) {
        this.serializedWeatherForecast = serializedWeatherForecastResponse;
    }

    public boolean isSuccessful() {
        try {
            JSONObject weatherForecastResponseJson = new JSONObject(serializedWeatherForecast);
            String responseCode = responseCode(weatherForecastResponseJson);
            return responseCode.startsWith(CODE_PREFIX_SUCCESS) || responseCode.startsWith(CODE_PREFIX_REDIRECT);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    private String responseCode(JSONObject weatherForecastResponseJson) throws JSONException {
        return weatherForecastResponseJson.getString(KEY_CODE);
    }
}
