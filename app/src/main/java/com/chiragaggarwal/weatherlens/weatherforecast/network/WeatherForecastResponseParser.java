package com.chiragaggarwal.weatherlens.weatherforecast.network;

// WeatherForecastResponseParser knows how to convert a weather forecast response to
// a domain level weather forecast

import android.support.annotation.NonNull;

import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecastCondition;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class WeatherForecastResponseParser {
    private static final String KEY_FORECASTS_LIST = "list";
    public static final String KEY_FORECAST_TEMPERATURE = "temp";
    public static final String KEY_TEMPERATURE_MINIMUM = "min";
    private static final String KEY_TEMPERATURE_MAXIMUM = "max";
    public static final String KEY_FORECAST_DATE = "dt";
    public static final String KEY_FORECAST_WEATHER = "weather";
    private static final String KEY_TEMPERATURE_DAY = "day";
    public static final int MULTIPLIER_MILLISECONDS = 1000;
    private String weatherForecastResponseJson;

    public WeatherForecastResponseParser(String weatherForecastResponseJson) {
        this.weatherForecastResponseJson = weatherForecastResponseJson;
    }

    public List<WeatherForecast> parseWeatherForecasts() {
        try {
            JSONObject weatherForecastResponseJsonObject = new JSONObject(weatherForecastResponseJson);
            JSONArray weatherForecastsCollection = weatherForecastResponseJsonObject.getJSONArray(KEY_FORECASTS_LIST);
            return buildDomainLevelWeatherForecasts(weatherForecastsCollection);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new ArrayList<>();
    }

    private List<WeatherForecast> buildDomainLevelWeatherForecasts(JSONArray weatherForecastsCollection) throws JSONException {
        List<WeatherForecast> weatherForecasts = new ArrayList<>();
        for (int weatherForecastIndex = 0; weatherForecastIndex < weatherForecastsCollection.length(); weatherForecastIndex++) {
            JSONObject weatherForecastJsonObject = weatherForecastsCollection.getJSONObject(weatherForecastIndex);
            WeatherForecast weatherForecast = buildDomainLevelWeatherForecast(weatherForecastJsonObject);
            weatherForecasts.add(weatherForecast);
        }
        return weatherForecasts;
    }

    private WeatherForecast buildDomainLevelWeatherForecast(JSONObject weatherForecastJsonObject) throws JSONException {
        long timeStampInSeconds = weatherForecastJsonObject.getLong(KEY_FORECAST_DATE);
        JSONObject temperatures = weatherForecastJsonObject.getJSONObject(KEY_FORECAST_TEMPERATURE);
        JSONArray weatherInformations = weatherForecastJsonObject.getJSONArray(KEY_FORECAST_WEATHER);
        JSONObject weatherInformation = weatherInformations.getJSONObject(0);
        WeatherForecastConditionParser weatherForecastConditionParser = new WeatherForecastConditionParser(weatherInformation);
        WeatherForecastCondition weatherForecastCondition = weatherForecastConditionParser.parseQuality();
        String weatherForecastDescription = weatherForecastConditionParser.parseDescription();
        return buildParsedDomainLevelWeatherForecast(timeStampInSeconds, temperatures, weatherForecastCondition, weatherForecastDescription);
    }

    @NonNull
    private WeatherForecast buildParsedDomainLevelWeatherForecast(long timeStampInSeconds, JSONObject temperatures, WeatherForecastCondition weatherForecastCondition, String weatherForecastDescription) throws JSONException {
        double minimumTemperature = temperatures.getDouble(KEY_TEMPERATURE_MINIMUM);
        double maximumTemperature = temperatures.getDouble(KEY_TEMPERATURE_MAXIMUM);
        double dayTemperature = temperatures.getDouble(KEY_TEMPERATURE_DAY);
        Date date = new Date();
        date.setTime(timeStampInSeconds * MULTIPLIER_MILLISECONDS);
        return new WeatherForecast(date, dayTemperature, maximumTemperature, minimumTemperature, weatherForecastCondition, weatherForecastDescription);
    }
}
