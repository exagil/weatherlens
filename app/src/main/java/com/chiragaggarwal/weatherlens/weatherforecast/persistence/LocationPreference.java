package com.chiragaggarwal.weatherlens.weatherforecast.persistence;

import android.content.Context;
import android.content.SharedPreferences;

import com.chiragaggarwal.weatherlens.R;

public class LocationPreference {
    private static final String KEY_LOCATION_PREFERENCE = "location_preference";
    private static final String KEY_CURRENT_LOCATION = "current_location";
    private SharedPreferences sharedPreferences;
    private Context context;

    public LocationPreference(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(KEY_LOCATION_PREFERENCE, Context.MODE_PRIVATE);
    }

    public String current() {
        return sharedPreferences.getString(KEY_CURRENT_LOCATION, context.getString(R.string.location_default));
    }

    public void set(String locationName) {
        sharedPreferences.edit().putString(KEY_CURRENT_LOCATION, locationName).commit();
    }

    public void clear() {
        sharedPreferences.edit().clear().commit();
    }
}
