package com.chiragaggarwal.weatherlens.weatherforecast.view;

import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;

import java.util.List;

public interface WeatherForecastView {
    void showProgressBar();

    void hideProgressBar();

    void onForecomingWeatherForecastsLoaded(List<WeatherForecast> forecomingWeatherforecasts);

    void onForecastsLoadFailed(String errorMessage);

    void onTodaysWeatherForecastLoaded(WeatherForecast todaysWeatherForecast);

    void showForecastsLayout();

    void hideNoForecastsText();

    void hideForecastsLayout();

    void showNoForecastsText();
}
