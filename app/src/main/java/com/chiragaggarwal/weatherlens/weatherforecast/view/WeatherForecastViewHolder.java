package com.chiragaggarwal.weatherlens.weatherforecast.view;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.chiragaggarwal.weatherlens.R;

public class WeatherForecastViewHolder extends RecyclerView.ViewHolder {
    private final TextView textTemperatureMinimum;
    private final TextView textTemperatureMaximum;
    private final ImageView imageMood;
    private final TextView textDay;

    public WeatherForecastViewHolder(View itemView) {
        super(itemView);
        imageMood = (ImageView) itemView.findViewById(R.id.image_mood);
        textTemperatureMinimum = (TextView) itemView.findViewById(R.id.text_temperature_minimum);
        textTemperatureMaximum = (TextView) itemView.findViewById(R.id.text_temperature_maximum);
        textDay = (TextView) itemView.findViewById(R.id.text_day);
    }

    public void bind(String minimumTemperature, String maximumTemperature, int imageMoodResource, String formattedDay) {
        textTemperatureMaximum.setText(maximumTemperature);
        textTemperatureMinimum.setText(minimumTemperature);
        imageMood.setImageResource(imageMoodResource);
        textDay.setText(formattedDay);
    }
}
