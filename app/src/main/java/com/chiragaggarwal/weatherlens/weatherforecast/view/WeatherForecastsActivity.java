package com.chiragaggarwal.weatherlens.weatherforecast.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.chiragaggarwal.weatherlens.R;
import com.chiragaggarwal.weatherlens.WeatherLensApplication;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;
import com.chiragaggarwal.weatherlens.weatherforecast.network.WeatherForecastClient;
import com.chiragaggarwal.weatherlens.weatherforecast.persistence.LocationPreference;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;

import java.util.List;

import javax.inject.Inject;

public class WeatherForecastsActivity extends AppCompatActivity implements WeatherForecastView, PlaceSelectionListener {
    public static final String BLANK = "";
    public static final int GRID_SPAN_COUNT = 5;
    private WeatherForecastsPresenter weatherForecastsPresenter;
    private RecyclerView listWeatherForecasts;
    private WeatherForecastsAdapter weatherForecastsAdapter;
    private TextView textMinimumTemperature;
    private TextView textMaximumTemperature;
    private Toolbar toolbarSearch;
    private PlaceAutocompleteFragment placeAutocompleteFragment;
    private ProgressBar progressBar;
    private TextView textNoForecasts;
    private View layoutForecasts;
    private TextView textWeatherDescription;
    private ImageView imageMoodToday;
    private TextView textLocationName;
    private TextView textTemperatureDay;

    @Inject
    public WeatherForecastClient weatherForecastClient;

    @Inject
    public LocationPreference locationPreference;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecasts);
        ((WeatherLensApplication) getApplication()).getWeatherLensDependencies().inject(this);
        initializeViews();
        weatherForecastsPresenter.getForecasts();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(ProgressBar.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(ProgressBar.GONE);
    }

    @Override
    public void onForecomingWeatherForecastsLoaded(List<WeatherForecast> forecomingWeatherforecasts) {
        weatherForecastsAdapter.swap(forecomingWeatherforecasts);
    }

    @Override
    public void onTodaysWeatherForecastLoaded(WeatherForecast todaysWeatherForecast) {
        textMaximumTemperature.setText(todaysWeatherForecast.maximumTemperature());
        textMinimumTemperature.setText(todaysWeatherForecast.minimumTemperature());
        textWeatherDescription.setText(todaysWeatherForecast.weatherForecastDescription);
        imageMoodToday.setImageResource(todaysWeatherForecast.todaysMoodResource());
        textTemperatureDay.setText(todaysWeatherForecast.dayTemperature());
        textLocationName.setText(locationPreference.current());
    }

    @Override
    public void showForecastsLayout() {
        layoutForecasts.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNoForecastsText() {
        textNoForecasts.setVisibility(TextView.GONE);
    }

    @Override
    public void hideForecastsLayout() {
        layoutForecasts.setVisibility(View.GONE);
    }

    @Override
    public void showNoForecastsText() {
        textNoForecasts.setVisibility(TextView.VISIBLE);
    }

    @Override
    public void onForecastsLoadFailed(String errorMessage) {
        textNoForecasts.setText(errorMessage);
    }

    @Override
    public void onPlaceSelected(Place place) {
        String placeName = place.getName().toString();
        locationPreference.set(placeName);
        weatherForecastsPresenter.getForecasts();
    }

    @Override
    public void onError(Status status) {
        String statusMessage = status.getStatusMessage();
        hideForecastsLayout();
        showNoForecastsText();
        textNoForecasts.setText(statusMessage);
    }

    private void initializeViews() {
        setupWeatherForecastViews();
        setupPlacesAutocomplete();
        setupToolbar();
    }

    private void setupWeatherForecastViews() {
        setupTodaysForecastView();
        setupForecomingWeatherForecastsList();
        weatherForecastsPresenter = new WeatherForecastsPresenter(this, weatherForecastClient);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        textNoForecasts = (TextView) findViewById(R.id.text_no_forecasts);
    }

    private void setupPlacesAutocomplete() {
        AutocompleteFilter placesAutocompleteFilter = new AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_REGIONS).build();
        placeAutocompleteFragment = (PlaceAutocompleteFragment) getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        placeAutocompleteFragment.setFilter(placesAutocompleteFilter);
        placeAutocompleteFragment.setHint(this.getString(R.string.search_city));
        placeAutocompleteFragment.setOnPlaceSelectedListener(this);
        placeAutocompleteFragment.setText(locationPreference.current());
    }

    private void setupTodaysForecastView() {
        textMinimumTemperature = (TextView) findViewById(R.id.text_minimum_temperature);
        textMaximumTemperature = (TextView) findViewById(R.id.text_maximum_temperature);
        textWeatherDescription = (TextView) findViewById(R.id.text_weather_description);
        imageMoodToday = (ImageView) findViewById(R.id.image_mood_today);
        textLocationName = (TextView) findViewById(R.id.text_location_name);
        textTemperatureDay = (TextView) findViewById(R.id.text_temperature_day);
    }

    private void setupForecomingWeatherForecastsList() {
        layoutForecasts = findViewById(R.id.layout_forecasts);
        GridLayoutManager weatherForecastsGridLayoutManager = new GridLayoutManager(this, GRID_SPAN_COUNT, GridLayoutManager.VERTICAL, false);
        listWeatherForecasts = (RecyclerView) findViewById(R.id.list_weather_forecasts);
        listWeatherForecasts.setLayoutManager(weatherForecastsGridLayoutManager);
        weatherForecastsAdapter = new WeatherForecastsAdapter(this);
        listWeatherForecasts.setAdapter(weatherForecastsAdapter);
    }

    private void setupToolbar() {
        toolbarSearch = (Toolbar) findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbarSearch);
        getSupportActionBar().setTitle(BLANK);
    }
}
