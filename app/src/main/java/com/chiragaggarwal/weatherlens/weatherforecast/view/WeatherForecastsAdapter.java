package com.chiragaggarwal.weatherlens.weatherforecast.view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chiragaggarwal.weatherlens.R;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;

import java.util.ArrayList;
import java.util.List;

public class WeatherForecastsAdapter extends RecyclerView.Adapter<WeatherForecastViewHolder> {
    private ArrayList<WeatherForecast> weatherForecasts;
    private Context context;

    public WeatherForecastsAdapter(Context context) {
        this.context = context;
        this.weatherForecasts = new ArrayList<>();
    }

    @Override
    public WeatherForecastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItemWeatherForecast = LayoutInflater.from(context).inflate(R.layout.list_item_weather_forecast, parent, false);
        return new WeatherForecastViewHolder(listItemWeatherForecast);
    }

    @Override
    public void onBindViewHolder(WeatherForecastViewHolder holder, int position) {
        WeatherForecast weatherForecast = weatherForecasts.get(position);
        holder.bind(weatherForecast.minimumTemperature(), weatherForecast.maximumTemperature(), weatherForecast.moodResource(), weatherForecast.formattedDay());
    }

    @Override
    public int getItemCount() {
        return weatherForecasts.size();
    }

    public void swap(List<WeatherForecast> forecasts) {
        this.weatherForecasts.clear();
        this.weatherForecasts.addAll(forecasts);
        this.notifyDataSetChanged();
    }
}
