package com.chiragaggarwal.weatherlens.weatherforecast.view;

import com.chiragaggarwal.weatherlens.common.Callback;
import com.chiragaggarwal.weatherlens.common.NetworkError;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;
import com.chiragaggarwal.weatherlens.weatherforecast.network.WeatherForecastClient;

import java.util.List;

public class WeatherForecastsPresenter {

    private final WeatherForecastView weatherForecastView;
    private final WeatherForecastClient weatherForecastClient;

    public WeatherForecastsPresenter(WeatherForecastView weatherForecastView, WeatherForecastClient weatherForecastClient) {
        this.weatherForecastView = weatherForecastView;
        this.weatherForecastClient = weatherForecastClient;
    }

    public void getForecasts() {
        weatherForecastView.showProgressBar();
        weatherForecastClient.getWeatherForecasts(new Callback<List<WeatherForecast>, NetworkError>() {
            @Override
            public void onSuccess(List<WeatherForecast> weatherForecasts) {
                WeatherForecast todaysWeatherForecast = weatherForecasts.get(0);
                weatherForecasts.remove(todaysWeatherForecast);
                weatherForecastView.showForecastsLayout();
                weatherForecastView.hideNoForecastsText();
                weatherForecastView.onForecomingWeatherForecastsLoaded(weatherForecasts);
                weatherForecastView.onTodaysWeatherForecastLoaded(todaysWeatherForecast);
                weatherForecastView.hideProgressBar();
            }

            @Override
            public void onFailure(NetworkError networkError) {
                weatherForecastView.hideForecastsLayout();
                weatherForecastView.showNoForecastsText();
                weatherForecastView.onForecastsLoadFailed(networkError.getMessage());
                weatherForecastView.hideProgressBar();
            }
        });
    }
}
