package com.chiragaggarwal.weatherlens.common;

import android.content.Context;

import com.chiragaggarwal.weatherlens.R;

import junit.framework.Assert;

import org.junit.Test;
import org.mockito.Mockito;

import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

public class NetworkErrorTest {
    @Test
    public void testThatNoInternetConnectionMessageIsDeterminedWhenCannotConnectToHost() {
        Context applicationContext = Mockito.mock(Context.class);
        Mockito.when(applicationContext.getString(R.string.error_no_internet_connection)).thenReturn("No Internet Connection");
        Throwable throwable = new UnknownHostException("http://api.openweathermap.org/");
        NetworkError networkError = new NetworkError(applicationContext, throwable);
        Assert.assertEquals("No Internet Connection", networkError.getMessage());
    }

    @Test
    public void testThatNoInternetConnectionMessageIsDeterminedWhenWaitedTooLongToGetAResponse() {
        Context applicationContext = Mockito.mock(Context.class);
        Mockito.when(applicationContext.getString(R.string.error_no_internet_connection)).thenReturn("No Internet Connection");
        Throwable throwable = new SocketTimeoutException("Could not connect to http://api.openweathermap.org/");
        NetworkError networkError = new NetworkError(applicationContext, throwable);
        Assert.assertEquals("No Internet Connection", networkError.getMessage());
    }
}
