package com.chiragaggarwal.weatherlens.testhelper;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import rx.Scheduler;
import rx.android.plugins.RxAndroidPlugins;
import rx.android.plugins.RxAndroidSchedulersHook;
import rx.plugins.RxJavaPlugins;
import rx.plugins.RxJavaSchedulersHook;
import rx.schedulers.Schedulers;

public class RxSchedulersOverrideRule implements TestRule {
    @Override
    public Statement apply(Statement base, Description description) {
        return new RxOverrideRuleStatement(base);
    }

    RxJavaSchedulersHook rxJavaSchedulersHook = new RxJavaSchedulersHook() {
        @Override
        public Scheduler getIOScheduler() {
            return Schedulers.immediate();
        }
    };

    RxAndroidSchedulersHook rxAndroidSchedulersHook = new RxAndroidSchedulersHook() {
        @Override
        public Scheduler getMainThreadScheduler() {
            return Schedulers.immediate();
        }
    };

    private class RxOverrideRuleStatement extends Statement {
        private Statement baseStatement;

        public RxOverrideRuleStatement(Statement baseStatement) {
            this.baseStatement = baseStatement;
        }

        @Override
        public void evaluate() throws Throwable {
            RxAndroidPlugins.getInstance().reset();
            RxJavaPlugins.getInstance().reset();
            RxAndroidPlugins.getInstance().registerSchedulersHook(rxAndroidSchedulersHook);
            RxJavaPlugins.getInstance().registerSchedulersHook(rxJavaSchedulersHook);
            baseStatement.evaluate();
            RxAndroidPlugins.getInstance().reset();
            RxJavaPlugins.getInstance().reset();

        }
    }
}
