package com.chiragaggarwal.weatherlens.weatherforecast.models;

import com.chiragaggarwal.weatherlens.R;

import junit.framework.Assert;

import org.junit.Test;

public class WeatherForecastConditionTest {
    @Test
    public void testThatGoodWeatherForecastConditionHasAHappyMoodResource() {
        Assert.assertEquals(R.drawable.icon_happy, WeatherForecastCondition.GOOD.moodResource());
    }

    @Test
    public void testThatBadWeatherForecastConditionHasASadMoodResource() {
        Assert.assertEquals(R.drawable.icon_sad, WeatherForecastCondition.BAD.moodResource());
    }

    @Test
    public void testThatAverageWeatherForecastConditionHasAWhateverMoodResource() {
        Assert.assertEquals(R.drawable.icon_whatever, WeatherForecastCondition.AVERAGE.moodResource());
    }

    @Test
    public void testThatTodaysGoodWeatherForecastConditionHasAHappyMoodResource() {
        Assert.assertEquals(R.drawable.icon_today_happy, WeatherForecastCondition.GOOD.todaysMoodResource());
    }

    @Test
    public void testThatTodaysBadWeatherForecastConditionHasABadMoodResource() {
        Assert.assertEquals(R.drawable.icon_today_sad, WeatherForecastCondition.BAD.todaysMoodResource());
    }

    @Test
    public void testThatTodaysAverageWeatherForecastConditionHasAnAverageMoodResource() {
        Assert.assertEquals(R.drawable.icon_today_whatever, WeatherForecastCondition.AVERAGE.todaysMoodResource());
    }
}
