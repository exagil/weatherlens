package com.chiragaggarwal.weatherlens.weatherforecast.models;

import com.chiragaggarwal.weatherlens.R;

import junit.framework.Assert;

import org.junit.Test;

import java.util.Date;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

public class WeatherForecastTest {
    @Test
    public void testThatWeatherForecastIsNotEqualToNull() {
        Date date = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(date, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertFalse(weatherForecast.equals(null));
    }

    @Test
    public void testThatWeatherForecastIsItself() {
        Date date = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(date, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertTrue(weatherForecast.equals(weatherForecast));
    }

    @Test
    public void testThatAWeatherForecastIsEqualToAnotherWeatherForecastWithSameStates() {
        Date date = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(date, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        WeatherForecast otherWeatherForecast = new WeatherForecast(date, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertTrue(weatherForecast.equals(otherWeatherForecast));
    }

    @Test
    public void testThatAWeatherForecastIsNotEqualToAnotherWeatherForecastWithDifferentStates() {
        Date firstWeatherForecastDate = new Date();
        Date secondWeatherForecastDate = new Date();
        secondWeatherForecastDate.setTime(System.currentTimeMillis() - 10000);
        WeatherForecast weatherForecast = new WeatherForecast(firstWeatherForecastDate, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        WeatherForecast otherWeatherForecast = new WeatherForecast(secondWeatherForecastDate, 24.98d, 29.31d, 21.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertFalse(weatherForecast.equals(otherWeatherForecast));
    }

    @Test
    public void testThatWeatherForecastIsNotEqualToAnythingOtherThanAWeatherForecast() {
        Date date = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(date, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertFalse(weatherForecast.equals(new Object()));
    }

    @Test
    public void testThatTwoEqualWeatherForecastsHaveEqualHashCodes() {
        Date firstWeatherForecastDate = new Date();
        WeatherForecast weatherForecast = new WeatherForecast(firstWeatherForecastDate, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        WeatherForecast thatWeatherForecast = new WeatherForecast(firstWeatherForecastDate, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertEquals(weatherForecast.hashCode(), thatWeatherForecast.hashCode());
    }

    @Test
    public void testThatWeatherForecastKnowsItsMinimumTemperature() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertEquals("20°", weatherForecast.minimumTemperature());
    }

    @Test
    public void testThatWeatherForecastKnowsItsMaximumTemperature() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertEquals("24°", weatherForecast.maximumTemperature());
    }

    @Test
    public void testThatWeatherForecastKnowsItsDayTemperature() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Moderate Rain");
        assertEquals("22°", weatherForecast.dayTemperature());
    }

    @Test
    public void testThatGoodWeatherForecastConditionHasAHappyMoodResource() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.GOOD, "Moderate Rain");
        Assert.assertEquals(R.drawable.icon_happy, weatherForecast.moodResource());
    }

    @Test
    public void testThatBadWeatherForecastConditionHasASadMoodResource() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.BAD, "Volcanic Ash");
        Assert.assertEquals(R.drawable.icon_sad, weatherForecast.moodResource());
    }

    @Test
    public void testThatAverageWeatherForecastConditionHasAWhateverMoodResource() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals(R.drawable.icon_whatever, weatherForecast.moodResource());
    }

    @Test
    public void testThatTodaysGoodWeatherForecastConditionHasAHappyMoodResource() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.GOOD, "Moderate Rain");
        Assert.assertEquals(R.drawable.icon_today_happy, weatherForecast.todaysMoodResource());
    }

    @Test
    public void testThatTodaysBadWeatherForecastConditionHasASadMoodResource() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.BAD, "Volcanic Ash");
        Assert.assertEquals(R.drawable.icon_today_sad, weatherForecast.todaysMoodResource());
    }

    @Test
    public void testThatTodaysAverageWeatherForecastConditionHasAWhateverMoodResource() {
        WeatherForecast weatherForecast = new WeatherForecast(new Date(), 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals(R.drawable.icon_today_whatever, weatherForecast.todaysMoodResource());
    }

    @Test
    public void testThatItKnowsIfItIsForMonday() {
        Date dateOfMonday = new Date();
        dateOfMonday.setTime(1473057871000l);
        WeatherForecast weatherForecast = new WeatherForecast(dateOfMonday, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals("Mon", weatherForecast.formattedDay());
    }

    @Test
    public void testThatItKnowsIfItIsForTuesday() {
        Date dateOfMonday = new Date();
        dateOfMonday.setTime(1473144271000l);
        WeatherForecast weatherForecast = new WeatherForecast(dateOfMonday, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals("Tue", weatherForecast.formattedDay());
    }

    @Test
    public void testThatItKnowsIfItIsForWednesday() {
        Date dateOfMonday = new Date();
        dateOfMonday.setTime(1473230671000l);
        WeatherForecast weatherForecast = new WeatherForecast(dateOfMonday, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals("Wed", weatherForecast.formattedDay());
    }

    @Test
    public void testThatItKnowsIfItIsForThursday() {
        Date dateOfMonday = new Date();
        dateOfMonday.setTime(1473317071000l);
        WeatherForecast weatherForecast = new WeatherForecast(dateOfMonday, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals("Thu", weatherForecast.formattedDay());
    }

    @Test
    public void testThatItKnowsIfItIsForFriday() {
        Date dateOfMonday = new Date();
        dateOfMonday.setTime(1473403471000l);
        WeatherForecast weatherForecast = new WeatherForecast(dateOfMonday, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals("Fri", weatherForecast.formattedDay());
    }

    @Test
    public void testThatItKnowsIfItIsForSaturday() {
        Date dateOfMonday = new Date();
        dateOfMonday.setTime(1473489871000l);
        WeatherForecast weatherForecast = new WeatherForecast(dateOfMonday, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals("Sat", weatherForecast.formattedDay());
    }

    @Test
    public void testThatItKnowsIfItIsForSunday() {
        Date dateOfMonday = new Date();
        dateOfMonday.setTime(1473576271000l);
        WeatherForecast weatherForecast = new WeatherForecast(dateOfMonday, 22.24d, 24.81d, 20.5d, WeatherForecastCondition.AVERAGE, "Something");
        Assert.assertEquals("Sun", weatherForecast.formattedDay());
    }
}
