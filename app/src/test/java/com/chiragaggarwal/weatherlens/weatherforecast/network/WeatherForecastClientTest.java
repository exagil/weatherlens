package com.chiragaggarwal.weatherlens.weatherforecast.network;

import android.content.Context;

import com.chiragaggarwal.weatherlens.common.Callback;
import com.chiragaggarwal.weatherlens.common.NetworkError;
import com.chiragaggarwal.weatherlens.testhelper.RxSchedulersOverrideRule;
import com.chiragaggarwal.weatherlens.weatherforecast.persistence.LocationPreference;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import java.net.UnknownHostException;

import rx.Observable;

public class WeatherForecastClientTest {
    private Callback weatherForecastCallback;
    private ForecastNetworkService forecastNetworkService;
    private Context context;

    @Rule
    public RxSchedulersOverrideRule rxSchedulersOverrideRule = new RxSchedulersOverrideRule();

    @Before
    public void setup() {
        context = Mockito.mock(Context.class);
        forecastNetworkService = Mockito.mock(ForecastNetworkService.class);
        weatherForecastCallback = Mockito.mock(Callback.class);
    }

    @Test
    public void testThatItFailsWithValidNetworkErrorOnFailure() throws InterruptedException {
        Throwable throwable = new UnknownHostException("http://api.openweathermap.com/");
        Mockito.doReturn(Observable.error(throwable)).when(forecastNetworkService).getForecasts("validLocation");
        NetworkError networkError = new NetworkError(context, throwable);
        LocationPreference locationPreference = Mockito.mock(LocationPreference.class);
        Mockito.when(locationPreference.current()).thenReturn("validLocation");
        WeatherForecastClient weatherForecastClient = new WeatherForecastClient(context, forecastNetworkService, locationPreference);
        weatherForecastClient.getWeatherForecasts(weatherForecastCallback);
        Thread.sleep(1000);
        Mockito.verify(weatherForecastCallback).onFailure(networkError);
    }
}
