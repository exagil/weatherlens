package com.chiragaggarwal.weatherlens.weatherforecast.view;

import android.content.Context;

import com.chiragaggarwal.weatherlens.R;
import com.chiragaggarwal.weatherlens.common.Callback;
import com.chiragaggarwal.weatherlens.common.NetworkError;
import com.chiragaggarwal.weatherlens.testhelper.Fixture;
import com.chiragaggarwal.weatherlens.weatherforecast.models.WeatherForecast;
import com.chiragaggarwal.weatherlens.weatherforecast.network.WeatherForecastClient;

import org.junit.Test;
import org.mockito.Matchers;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.net.UnknownHostException;
import java.util.List;

public class WeatherForecastsPresenterTest {
    @Test
    public void testThatTodaysWeatherForecastAndForecomingWeatherForecastsAreLoadedSuccessfullyWhenLocationExists() {
        WeatherForecastView weatherForecastView = Mockito.mock(WeatherForecastView.class);
        WeatherForecastClient weatherForecastClient = Mockito.mock(WeatherForecastClient.class);
        WeatherForecastsPresenter weatherForecastsPresenter = new WeatherForecastsPresenter(weatherForecastView, weatherForecastClient);
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                List<WeatherForecast> weatherForecasts = new Fixture("fixture_weather_forecasts.json").loadList(WeatherForecast[].class);
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onSuccess(weatherForecasts);
                return null;
            }
        }).when(weatherForecastClient).getWeatherForecasts(Matchers.<Callback>any());
        List<WeatherForecast> weatherForecasts = new Fixture("fixture_weather_forecasts.json").loadList(WeatherForecast[].class);
        WeatherForecast todaysWeatherForecast = weatherForecasts.get(0);
        weatherForecasts.remove(todaysWeatherForecast);
        weatherForecastsPresenter.getForecasts();
        Mockito.verify(weatherForecastView).showProgressBar();
        Mockito.verify(weatherForecastView).showForecastsLayout();
        Mockito.verify(weatherForecastView).hideNoForecastsText();
        Mockito.verify(weatherForecastView).onTodaysWeatherForecastLoaded(todaysWeatherForecast);
        Mockito.verify(weatherForecastView).onForecomingWeatherForecastsLoaded(weatherForecasts);
        Mockito.verify(weatherForecastView).hideProgressBar();
    }

    @Test
    public void testThatForecastsAreNotLoadedWhenSomeIssueExistsWithTheNetwork() {
        Context context = Mockito.mock(Context.class);
        Mockito.when(context.getString(R.string.error_no_internet_connection)).thenReturn("No Internet Connection");
        WeatherForecastView weatherForecastView = Mockito.mock(WeatherForecastView.class);
        WeatherForecastClient weatherForecastClient = Mockito.mock(WeatherForecastClient.class);
        WeatherForecastsPresenter weatherForecastsPresenter = new WeatherForecastsPresenter(weatherForecastView, weatherForecastClient);
        final NetworkError networkError = new NetworkError(context, new UnknownHostException("http://api.openweathermap.org"));
        Mockito.doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Callback callback = (Callback) invocation.getArguments()[0];
                callback.onFailure(networkError);
                return null;
            }
        }).when(weatherForecastClient).getWeatherForecasts(Matchers.<Callback>any());
        weatherForecastsPresenter.getForecasts();
        Mockito.verify(weatherForecastView).showProgressBar();
        Mockito.verify(weatherForecastView).hideForecastsLayout();
        Mockito.verify(weatherForecastView).showNoForecastsText();
        Mockito.verify(weatherForecastView).onForecastsLoadFailed("No Internet Connection");
        Mockito.verify(weatherForecastView).hideProgressBar();
    }
}
